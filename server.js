var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var fs = require('fs');
var path = require('path');
var NodeWebcam = require( "node-webcam" );

var spawn = require('child_process').spawn;
var proc;

app.use('/', express.static(path.join(__dirname, 'stream')));

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

var sockets = {};

io.on('connection', function(socket) {
    sockets[socket.id] = socket;
    console.log("Total clients connected : ", Object.keys(sockets).length);

    // socket.on('disconnect', function() {
    //     delete sockets[socket.id];

    //     // no more sockets, kill the stream
    //     if (Object.keys(sockets).length == 0) {
    //         app.set('watchingFile', false);
    //         if (proc) proc.kill();
    //         fs.unwatchFile('./stream/image_stream.jpg');
    //     }
    // });
    capture();
  fs.watch('./stream/', function(current, previous, next) {
        console.log('fitta');
        console.log('current', current);
        console.log('previous', previous);
        console.log('next', next);
        capture();
        io.sockets.emit('liveStream', previous);
    });

   
});

http.listen(3000, function() {
    console.log('listening on *:3000');
});

function stopStreaming() {
    if (Object.keys(sockets).length == 0) {
        app.set('watchingFile', false);
        if (proc) proc.kill();
        fs.unwatchFile('./stream/image_stream.jpg');
    }
}


function capture() {
    console.log('capture');

//Default options
 
    var opts = {
     
        //Picture related
        width: 1280,
        height: 720,
        quality: 100,
     
        //Delay to take shot
        delay: 1,

        //Save shots in memory
        saveShots: true,
     
        // [jpeg, png] support varies
        // Webcam.OutputTypes
        output: "jpeg",
     
        //Which camera to use
        //Use Webcam.list() for results
        //false for default device
        device: false,
     
        // [location, buffer, base64]
        // Webcam.CallbackReturnTypes
        callbackReturn: "location",
      
        //Logging
        verbose: false
     
        };
     
     

         
        //Also available for quick use  
        NodeWebcam.capture( "test_picture", opts, function( err, data ) {
            console.log('webcam', data);

            fs.writeFile('./stream/data2.jpg', data, 'binary', function(err){
                if (err) throw err
                console.log('File saved.2')
            })
        });

        // //Get list of cameras
        // Webcam.list( function( list ) {
        //     //Use another device
        //     var anotherCam = NodeWebcam.create( { device: list[ 0 ] } );
        // });
         
        // //Return type with base 64 image
        // var opts = {
        //     callbackReturn: "base64"
        // };
        // NodeWebcam.capture( "test_picture", opts, function( err, data ) {
        //     var image = "<img src='" + data + "'>";
        // });
}

function startStreaming(io) {
    console.log('startStreaming');

    // if (app.get('watchingFile')) {
    //     io.sockets.emit('liveStream', 'image_stream.jpg?_t=' + (Math.random() * 100000));
    //     return;
    // }

    // var args = ["-w", "640", "-h", "480", "-o", "./stream/image_stream.jpg", "-t", "999999999", "-tl", "100"];
    // proc = spawn('raspistill', args);

    // console.log('Watching for changes...');

    // app.set('watchingFile', true);

    // fs.watch('./stream', function(current, previous) {
    //     io.sockets.emit('liveStream', 'image_stream.jpg?_t=' + (Math.random() * 100000));
    // })

    // --------------------------------------

    // if (app.get('watchingFile')) {
    //     io.sockets.emit('liveStream', 'image_stream.jpg?_t=' + (Math.random() * 100000));
    //     return;
    // }

    // var args = ["-w", "640", "-h", "480", "-o", "./stream/image_stream.jpg", "-t", "999999999", "-tl", "100"];
    // proc = spawn('raspistill', args);

    // console.log('Watching for changes...');

    // app.set('watchingFile', true);

    // fs.watchFile('./stream/image_stream.jpg', function(current, previous) {
    //     io.sockets.emit('liveStream', 'image_stream.jpg?_t=' + (Math.random() * 100000));
    // })

    // --------------------------------------


  
    

}

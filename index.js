'use strict';
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
// const routes = require('./routes/index.js');


// app.use(routes);

app.use(bodyParser.json());

if (require.main === module) {
  app.listen(3000, () => {});
}

module.exports = app;
